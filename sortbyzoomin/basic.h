#pragma once

enum Status_code
{
	RANGE_ERROR = 0,
	ENTRY_FOUND = 1,
	SUCCESS = 2,
};
