#include "Sort.h"

void main()
{
	SortLib<int> sortInstance;
	//insert sort
	int insertSortArray[] = {18,8,56,9,68,8};
	sortInstance.InsertSort(insertSortArray,sizeof(insertSortArray)/sizeof(int));
	//shell sort
	int shellSortArray[] = {18,8,15,9,5,3,8,16};
	int incr[] = {4,2,1};
	sortInstance.ShellSort(shellSortArray,sizeof(shellSortArray)/sizeof(int),incr,sizeof(incr)/sizeof(int));
	//bubble sort
	int bubbleSortArray[] = {18,8,15,9,1,3,5,8};
	sortInstance.BubbleSort(bubbleSortArray,sizeof(bubbleSortArray)/sizeof(int));
	//quick sort
	int quickSortArray[] = {49,38,66,97,38,68,5,8};
	sortInstance.QuickSort(quickSortArray,sizeof(quickSortArray)/sizeof(int));
	//simple selection sort
	int simpleSelectionSortArray[] = {36,48,48,90,88,80,76,99};
	sortInstance.SimpleSelectionSort(simpleSelectionSortArray,sizeof(simpleSelectionSortArray)/sizeof(int));
	//heap sort
	int heapSortArray[] = {49,38,66,97,49,26,86,25};
	sortInstance.HeapSort(heapSortArray,sizeof(heapSortArray)/sizeof(int));
	//merge sort
	int mergeSortArray[] = {49,38,56,30,88,80,38};
	sortInstance.MergeSort(mergeSortArray,sizeof(mergeSortArray)/sizeof(int));
	//radix sort
	int radixSortArray[] = {27,91,01,97,17,23,72,25,05,67,84,07,21,31};
	sortInstance.RadixSort(radixSortArray,sizeof(radixSortArray)/sizeof(int),10,2);
	//bucket sort
	int bucketSortArray[] = {78, 17, 39, 26, 72, 94, 21, 12, 23, 91};   
	sortInstance.BucketSort(bucketSortArray, sizeof(bucketSortArray)/sizeof(int));   
	//count sort
	int countSortArray[] = {2,5,3,1,2,3,1,7,9,9,9,9};   
	sortInstance.CountSort(countSortArray, sizeof(countSortArray)/sizeof(int)); 

	system("pause");
	return;
}