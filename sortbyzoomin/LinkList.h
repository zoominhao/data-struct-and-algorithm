#pragma once
/************************************************************************/
/* author zoomin                                                        */
/* time  2014-07-12                                                     */
/************************************************************************/
#include "basic.h"

template<class LinkType>
struct Node
{
	//数据成员
	  LinkType data;                      //数据域
	  Node<LinkType> *next;                //指针域
    //构造函数
	  Node();
	  Node(LinkType item, Node<LinkType> *link=NULL);
};

template<class LinkType>
Node<LinkType>::Node( LinkType item, Node<LinkType> *link/*=NULL*/ )
{
	data = item;
	next = link;
}

template<class LinkType>
Node<LinkType>::Node()
{
	next = NULL;
}

//线性链表类
template<class LinkType>
class LinkList
{
protected:
	//链表实现的数据成员
	Node<LinkType> *head;       //头结点指针
	//辅助函数
	Node<LinkType> *GetElemPtr(int position) const; //返回指向position个结点的指针
	void Init();
public:
	//抽象数据类型方法声明及重载编译系统默认方法声明
	LinkList();
	~LinkList();

	int Length() const;                //求线性表长度
	bool Empty() const;                //判断线性表是否为空
	void Clear();                      //将线性表清空
	void Traverse(void(*Visit)(LinkType&));     //遍历线性表

	Status_code GetElem(int position, LinkType& e) const;
	Status_code SetElem(int position, const LinkType& e);
	Status_code Delete(int position, LinkType& e);
	Status_code Insert(int position, const LinkType& e);

	LinkList(const LinkList<LinkType> & copy);    //复制构造函数
	LinkList<LinkType> &operator=(const LinkList<LinkType>& Copy);   //赋值语句重载
};

template<class LinkType>
LinkList<LinkType> & LinkList<LinkType>::operator=( const LinkList<LinkType>& Copy )
{
	if (&Copy!=this)
	{
		int copyLength = copy.Length();
		LinkType e;
		Clear();
		for (int curPosition = 1; curPosition <= copyLength; curPosition++)
		{
			copy.GetElem(curPosition,e);
			Insert(Length()+1,e);
		}
	}
	return *this;
}

template<class LinkType>
LinkList<LinkType>::LinkList( const LinkList<LinkType> & copy )
{
	int copyLength = copy.Length();
	LinkType e;
	Init();
	for (int curPosition = 1; curPosition <= copyLength; curPosition++)
	{
		copy.GetElem(curPosition,e);
		Insert(Length()+1,e);
	}
}

template<class LinkType>
Status_code LinkList<LinkType>::Insert( int position, const LinkType& e )
{
	if (position < 1 || position > Length()+1)
	{
		return RANGE_ERROR;
	}
	else
	{
		Node<LinkType> *tmpPtr;
		tmpPtr = GetElemPtr(position-1);
		Node<LinkType> *newPtr;
		newPtr = new Node<LinkType>(e,tmpPtr->next);
		tmpPtr->next = newPtr;
		return SUCCESS;
	}
}

template<class LinkType>
Status_code LinkList<LinkType>::Delete( int position, LinkType& e )
{
	if (position < 1 || position > Length())
	{
		return RANGE_ERROR;
	}
	else
	{
		Node<LinkType>* tmpPtr;
		tmpPtr = GetElemPtr(position-1);
		Node<LinkType>* nextPtr = tmpPtr->next;
		tmpPtr->next = nextPtr->next;
		e = nextPtr->data;
		delete nextPtr;
		return SUCCESS;
	}
}

template<class LinkType>
Status_code LinkList<LinkType>::SetElem( int position, const LinkType& e )
{
	if (position < 1 || position > Lenght())
	{
		return RANGE_ERROR;
	}
	else
	{
		Node<LinkType> *tmpPtr;
		tmpPtr = GetElemPtr(position);
		tmpPtr->data = e;
		return SUCCESS;
	}
}

template<class LinkType>
Status_code LinkList<LinkType>::GetElem( int position, LinkType& e ) const
{
	if (position < 1 || position > Lenght())
	{
		 return RANGE_ERROR;
	}
	else
	{
		Node<LinkType> *tmpPtr;
		tmpPtr = GetElemPtr(position);
		e = tmpPtr->data;
		return ENTRY_FOUND;
	}
}

template<class LinkType>
void LinkList<LinkType>::Traverse( void(*Visit)(LinkType&) )
{
	for (Node<LinkType> *tmpPtr = head->next; tmpPtr!=NULL; tmpPtr=tmpPtr->next)
	{
		(*Visit)(tmpPtr->data);    //对线性表的每个元素调用函数（*visit）
	}
}

template<class LinkType>
void LinkList<LinkType>::Clear()
{
	LinkType tmpElem;
	while (Length()>0)
	{
		Delete(1,tmpElem);
	}
}



template<class LinkType>
bool LinkList<LinkType>::Empty() const
{
	return head->next==NULL;
}

template<class LinkType>
int LinkList<LinkType>::Length() const
{
	int count = 0;
	for (Node<LinkType>* tmpPtr = head->next; tmpPtr!=NULL; tmpPtr=tmpPtr->next)
	{
		count++;
	}
	return count;
}

template<class LinkType>
LinkList<LinkType>::~LinkList()
{
	Clear();
	delete head;
}

template<class LinkType>
LinkList<LinkType>::LinkList()
{
	Init();
}

template<class LinkType>
void LinkList<LinkType>::Init()
{
	head = new Node<LinkType>;    //构造头指针
}

template<class LinkType>
Node<LinkType> * LinkList<LinkType>::GetElemPtr( int position ) const
{
	Node<LinkType>  *tmpPtr = head;
	int curPosition = 0;
	while (tmpPtr != NULL && curPosition < position)
	{
		tmpPtr = tmpPtr->next;
		curPosition++;
	}
	if (tmpPtr != NULL && curPosition == position)
		return tmpPtr;
	else
		return NULL;
}


