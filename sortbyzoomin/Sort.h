#pragma once
/************************************************************************/
/* author zoomin                                                        */
/* time  2014-07-12                                                     */
/************************************************************************/
#include <iostream>
#include <time.h>
#include <string>
#include <vector>
#include "LinkList.h"

using namespace std;

template <class SortType>
//升序排列
class SortLib
{
public:
	SortLib();
	~SortLib();

	//插入类排序
	void InsertSort(SortType* pData, int n);
	void ShellSort(SortType* pData, int n, int* inc, int t);
	//交换类排序
	void BubbleSort(SortType* pData, int n);
	void BubbleSort1(SortType* pData, int n);   //改进
	void BubbleSort2(SortType* pData, int n);
	void QuickSort(SortType* pData, int n);
	//选择类排序
	void SimpleSelectionSort(SortType* pData, int n);
	void HeapSort(SortType* pData, int n);
	//归并排序，易懂的高级排序算法
	void MergeSort(SortType* pData, int n);
	//基数排序
	void RadixSort(SortType* pData, int n, int r ,int d);

	//其它的一些排序
	  //桶排序
	void BucketSort(SortType* pData, int n);
	  //计数排序
	void CountSort(SortType* pData, int n);

private:
	void Distribute(SortType* pData, int n, int r, int d, int i, LinkList<SortType>* list);
	void Collect(SortType* pData, int n, int r, int d, int i, LinkList<SortType>* list);
	void Merge(SortType* pData, int low, int mid, int high);
	void MSort(SortType* pData, int low, int high);
	void SiftAdjust(SortType* pData, int low, int high);
	int Partition(SortType* pData, int low, int high);
	void QSort(SortType* pData, int low, int high);
	void ShellInsert(SortType* pData, int n, int incr);
	void display(SortType* pData, int n );
	void Swap(SortType& a, SortType& b);
	void StartSort(string sortName, SortType* pData, int n);
	void EndSort(SortType* pData, int n);
	int startTime;
	int endTime;
	int count;    //记录总循环次数
};

//时间复杂度O(n) 稳定排序
//原理：计数排序的思想是对每一个输入元素x，确定出小于x的元素个数，有了这一信息，就可以把x直接放在它在最终输出数组的位置上。当几个元素相同是，方案要略作修改。
template <class SortType>
void SortLib<SortType>::CountSort( SortType* pData, int n)
{
	StartSort("Count Sort!",pData,n);
	int max, min, k;
	max = min = pData[0]; 
	for (int i = 1; i < n; i++) 
	{   
		if (pData[i] > max) 
		{   
			max = pData[i];   
		}
		else if (pData[i] < min) 
		{   
			min = pData[i];   
		}   
	}   
	k = max - min + 1;

	int* C = (int*) malloc(sizeof(int) * k);
	memset(C, 0 , sizeof(int) * k);
	int value, pos;
	for(int i = 0; i < k; i++)
	{
		C[i] = 0;
	}
	for(int i = 0; i< n; i++)
	{
		C[pData[i]-min]++;
	}
	for(int i = 1; i < k; i++)
	{
		C[i] = C[i] + C[i-1];
	}

	SortType* tmpData = (SortType*) malloc(sizeof(SortType) * n);
	memset(tmpData, 0, sizeof(SortType) * n);
	for(int i=n-1; i>=0; i--)
	{
		value = pData[i];
		pos = C[value-min];
		tmpData[pos-1] = value;
		C[value-min]--;   //重复的数
	}
	pData = tmpData;
    EndSort(pData,n);
}

template<class SortType>
struct Barrel
{
	SortType node[10];   //桶的size=10
	int count;
};

//时间复杂度O(n) 稳定排序
//原理：将数据按范围划分到各个桶中
template <class SortType>
void SortLib<SortType>::BucketSort( SortType* pData, int n )
{
	StartSort("Bucket Sort!",pData,n);
	int max, min, num, pos;

	max = min = pData[0]; 
	for (int i = 1; i < n; i++) 
	{   
		if (pData[i] > max) 
		{   
			max = pData[i];   
		}
		else if (pData[i] < min) 
		{   
			min = pData[i];   
		}   
	}   

	num = (max - min + 1)/10 + 1;
	struct Barrel<SortType> *pBarrel; 
	pBarrel = (struct Barrel<SortType>*)malloc(sizeof(struct Barrel<SortType>) * num);    //分配内存空间
	memset(pBarrel, 0, sizeof(struct Barrel<SortType>) * num);         //初始化为0

	for (int i = 0; i < n; ++i)
	{
		int k = (pData[i] - min + 1)/10;
		(pBarrel + k)->node[(pBarrel + k)->count]  = pData[i];   
		(pBarrel + k)->count++;  
	}

	pos = 0;
	for (int i = 0; i < num; i++)
	{
		QSort((pBarrel+i)->node, 0, (pBarrel+i)->count-1);/* sort node in every barrel */  

		for (int j = 0; j < (pBarrel+i)->count; j++) 
		{   
			pData[pos++] = (pBarrel+i)->node[j];   
		}   
	}
	free(pBarrel);

	EndSort(pData,n);
}

//时间复杂度O(d*n)) 稳定排序
//原理：将数字按位数划分出n个关键字，每次针对一个关键字进行排序，然后针对排序后的序列进行下一个关键字的排序，循环至所有关键字都使用过则排序完成。
//r为基数，d为关键字位数
//要求数字表述称标准形式 1=>01
template <class SortType>
void SortLib<SortType>::RadixSort( SortType* pData, int n, int r ,int d)
{
	StartSort("Radix Sort!",pData,n);
	LinkList<SortType> *list = new LinkList<SortType>[r];
	for (int i = 1; i <= d; i++)
	{
		Distribute(pData,n,r,d,i,list);
		Collect(pData,n,r,d,i,list);
	}
	delete []list;
	EndSort(pData,n);
}

template <class SortType>
void SortLib<SortType>::Collect( SortType* pData, int n, int r, int d, int i, LinkList<SortType>* list )
{
	for (int k = 0, j = 0; j < r; j++)
	{
		SortType tmpElem;
		while (!list[j].Empty())
		{
			list[j].Delete(1,tmpElem);
		    pData[k++] = tmpElem;
		}
	}
}

template <class SortType>
void SortLib<SortType>::Distribute( SortType* pData, int n, int r, int d, int i, LinkList<SortType>* list )
{
	int power = (int)pow(r,i-1);    //抑制warning
	for (int j = 0; j < n; j++)
	{
		int index = (pData[j]/power)%r;
		list[index].Insert(list[index].Length()+1,pData[j]);
	}
}

//最坏,最好，平均情况下的时间复杂度O(n*log(n)) 稳定排序, n大时较好
//原理：将原序列划分为有序的两个序列，然后利用归并算法进行合并，合并之后即为有序序列。  
//内部排序 2-路归并足矣   只有外部排序才需要多路归并

template <class SortType>
void SortLib<SortType>::MergeSort( SortType* pData, int n )
{
	StartSort("Merge Sort!",pData,n);
	MSort(pData,0,n-1);
	EndSort(pData,n);
}

template <class SortType>
void SortLib<SortType>::MSort( SortType* pData, int low, int high )
{
	if (low<high)
	{
		int mid = (low + high)/2;

		MSort(pData, low ,mid);   
		MSort(pData, mid+1,high);
		Merge(pData,low,mid,high);
	}
}

template <class SortType>
void SortLib<SortType>::Merge( SortType* pData, int low, int mid, int high )
{
	SortType* tmpArr;
	tmpArr = new SortType[high+1];
	int i,j,k;
	for (i = low, j = mid+1, k = low; i <= mid && j <= high; k++)
	{
		if (pData[i]<=pData[j])
		{
			tmpArr[k]=pData[i];
			i++;
		}
		else
		{
			tmpArr[k]=pData[j];
			j++;
		}
		count++;
	}
	for (; i <= mid; i++,k++)
	{
		tmpArr[k]=pData[i];
	}
	for (; j <= high; j++,k++)
	{
		tmpArr[k]=pData[j];
	}
	for (i = low; i <= high; i++)
	{
		pData[i] = tmpArr[i];
	}
	delete []tmpArr;
}




//最坏情况下的时间复杂度O(n*log(n)) 不稳定排序, n大时较好,只需一个元素的辅助存储空间
//原理：利用大顶堆或小顶堆思想，首先建立堆，输出堆顶元素，然后调整剩余元素为新堆
//无序序列初始堆，最后一个非终端节点的下标是 （n-2）/2
template <class SortType>
void SortLib<SortType>::HeapSort( SortType* pData, int n )
{
	StartSort("Heap Sort!",pData,n);
	int i;
	for (i = (n-2)/2; i >= 0; --i)
	{
		SiftAdjust(pData,i,n-1);
	}
	for (i = n-1; i > 0; --i)
	{
		Swap(pData[0],pData[i]);
		SiftAdjust(pData,0,i-1);
	}
	EndSort(pData,n);
}

template <class SortType>
void SortLib<SortType>::SiftAdjust( SortType* pData, int low, int high )
{
	for (int f = low,i = 2*low+1; i <= high; i = 2*i+1)
	{
		count++;
		if (i<high && pData[i]<pData[i+1])
			i++;
		if (pData[f]>=pData[i])
			break;
		Swap(pData[f],pData[i]);
		f=i;
	}
}

//时间复杂度为O(n*n) 不稳定排序, n小时较好
//原理：将序列划分为无序和有序区，寻找无序区中的最小值和无序区的首元素交换，有序区扩大一个，循环最终完成全部排序。
template <class SortType>
void SortLib<SortType>::SimpleSelectionSort( SortType* pData, int n )
{
	StartSort("Simple Selection Sort!",pData,n);
	for (int i = 0; i < n-1; ++i)
	{
		int lowIndex = i;
		for (int j = i+1; j < n; ++j)
		{
			if(pData[j]<pData[lowIndex])
				 lowIndex = j;
			count++;
		}
		Swap(pData[i],pData[lowIndex]);
	}
	EndSort(pData,n);
}


//时间复杂度为O(nlog(n)), 最差时间复杂度为O(n*n) 不稳定排序, n大时较好
//原理：不断寻找一个序列的中点，然后对中点左右的序列递归的进行排序，直至全部序列排序完成，使用了分治的思想。
//寻找一个关键字，比其小的放一侧，比其大的放另一侧
template <class SortType>
void SortLib<SortType>::QuickSort( SortType* pData, int n )
{
	StartSort("Quick Sort!",pData,n);
	QSort(pData,0,n-1);
	EndSort(pData,n);
}

template <class SortType>
void SortLib<SortType>::QSort( SortType* pData, int low, int high )
{
	if (low<high)
	{
		int pivotLoc = Partition(pData,low,high);
		QSort(pData,low,pivotLoc-1);
		QSort(pData,pivotLoc+1,high);
	}
}

template <class SortType>
int SortLib<SortType>::Partition( SortType* pData, int low, int high )
{
	while (low<high)
	{
		while (low<high&&pData[high]>=pData[low])
		{
			high--;
			count++;
		}
		Swap(pData[low],pData[high]);
		while (low<high&&pData[low]<=pData[high])
		{
			low++;
			count++;
		}
		Swap(pData[low],pData[high]);
	}
	return low;
}

//第二次优化
//在第一步优化的基础上发进一步思考：如果R[i..n-1]已是有序区间，上次的扫描区间是R[0..i]，记上次扫描时最后一次执行交换的位置为lastSwapPos，则
//lastSwapPos在0与i之间，不难发现R[0..lastSwapPos]区间也是有序的，否则这个区间也会发生交换；所以下次扫描区间就可以由R[0..i] 缩减到[0..lastSwapPos]。
template <class SortType>
void SortLib<SortType>::BubbleSort2( SortType* pData, int n )
{
	StartSort("Bubble Sort2!",pData, n);
	int lastSwapPos = n-1;
	int nlastSwapPos = n-1;
	for (int i = n-1; i > 0; i--)
	{
		lastSwapPos = nlastSwapPos;
		for (int j = 0; j < lastSwapPos; j++)
		{
			nlastSwapPos = 0;
			if (pData[j] > pData[j+1])
			{
				Swap(pData[j],pData[j+1]);
				nlastSwapPos = j;
			}
			count++;
		}
	}
	EndSort(pData, n);
}

//第一次优化
//如果上面代码中，里面一层循环在某次扫描中没有执行交换，则说明此时数组已经全部有序列，无需再扫描了。因此，增加一个标记，每次发生交换，就标记，如果某次循环完没有标记，则说明已经完成排序。
template <class SortType>
void SortLib<SortType>::BubbleSort1( SortType* pData, int n )
{
	StartSort("Bubble Sort1!",pData, n);
	
	for (int i = n-1; i > 0; i--)
	{
		bool bSwaped = false;
		for (int j = 0; j < i; j++)
		{
			if (pData[j] > pData[j+1])
			{
				Swap(pData[j],pData[j+1]);
				bSwaped = true;
			}
			count++;
		}
		if (!bSwaped)
		  break;
		
	}
	EndSort(pData, n);
}


//时间复杂度为O(n*n), 稳定排序, n小时较好，最简单的交换排序
//原理：将序列划分为无序和有序区，不断通过交换较大元素至无序区尾完成排序。
//要点：设计交换判断条件，提前结束以排好序的序列循环。
template <class SortType>
void SortLib<SortType>::BubbleSort(SortType* pData, int n)
{
	StartSort("Bubble Sort!",pData, n);
	for (int i = n-1; i > 0; i--)
	{
		for (int j = 0; j < i; j++)
		{
			if (pData[j] > pData[j+1])
			  Swap(pData[j],pData[j+1]);
			count++;
		}
	}
	EndSort(pData, n);
}



//时间复杂度为O(n的m次方 2>m>1)  不稳定排序
//原理：对插入排序的改进   
//  将整个待排序的序列分割成若干个子序列，分别进行直接插入排序，等整个序列中的记录“基本有序”时，再对全体记录进行一次直接插入排序
template <class SortType>
void SortLib<SortType>::ShellSort( SortType* pData, int n, int* inc, int t)
{
	StartSort("Shell Sort!",pData, n);
	for (int i = 0; i < t; ++i)
	{
		//理论上inc的最后一个值为1
		ShellInsert(pData,n,inc[i]);
	}
	EndSort(pData, n);
}

template <class SortType>
void SortLib<SortType>::ShellInsert( SortType* pData, int n, int incr )
{
	for (int i = incr; i < n; ++i)
	{
		for (int j = i; j > incr - 1; j -= incr)
		{
			if (pData[j-incr] > pData[j])
				Swap(pData[j-incr],pData[j]);
			else
                break;
			count++;
		}
	}
}

//时间复杂度为O(n*n)  稳定排序
//原理：将数组分为无序区和有序区两个区，然后不断将无序区的第一个元素按大小顺序插入到有序区中去，最终将所有无序区元素都移动到有序区完成排序。 
template <class SortType>
void SortLib<SortType>::InsertSort( SortType* pData, int n )
{
	StartSort("Insert Sort!",pData, n);
	for (int i = 1; i < n; ++i)
	{
		for (int j = i; j > 0; j--)
		{
			if (pData[j-1] > pData[j])
				Swap(pData[j],pData[j-1]);
			else
				break;
			count++;
		}
	}
	EndSort(pData, n);
}

template <class SortType>
SortLib<SortType>::SortLib()
{
	startTime = 0;
	endTime = 0;
}

template <class SortType>
SortLib<SortType>::~SortLib()
{

}

template <class SortType>
void SortLib<SortType>::display( SortType* pData, int n )
{
	for (int i = 0; i < n; ++i)
	{
		cout<<pData[i]<<" ";
	}
	cout<<std::endl;
}


template <class SortType>
void SortLib<SortType>::StartSort( string sortName, SortType* pData, int n  )
{
	count = 0;
	cout<<"----"<<sortName<<"----"<<endl;
	cout<<"Before Sort: ";
	display( pData, n );
	startTime = clock();
}

template <class SortType>
void SortLib<SortType>::EndSort(SortType* pData, int n)
{
	endTime = clock();
	cout<<"After Sort: ";
	display( pData, n );
	cout<<"Time Cost: "<<(startTime - endTime)<<"ms"<<std::endl;
	if (count)
		cout<<"Loop Count: "<<count<<std::endl;
	cout<<std::endl;
}

template <class SortType>
void SortLib<SortType>::Swap( SortType& a, SortType& b )
{
	SortType c;
	c = a;
	a = b;
	b = c;
}